package com.wuze.sprintboot.netty.entity;

/**
 * 封装请求路径
 *
 */
public class Path {

    private String path;
    private String method;

    public Path(String path, String method) {
        this.path = path;
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "Path{" +
                "path='" + path + '\'' +
                ", method='" + method + '\'' +
                '}';
    }
}
