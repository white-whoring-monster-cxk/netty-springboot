package com.wuze.sprintboot.netty.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE , ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface nettyController {
    /**
     * 请求路径
     * @return
     */
    String value() default "";

    /**
     * 支持的提交方式
     * @return
     */
    String method() default "GET";
}
