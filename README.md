# netty_springboot

#### 介绍
使用netty代替spring boot中的tomcat，在访问性能上基本持平


#### 安装教程

1.  通过连接下载压缩包https://gitee.com/white-whoring-monster-cxk/netty-springboot.git

#### 使用说明

1.  解压压缩包后用idea打开
2.  使用maven工具install到maven本地仓库
3.  查看pom文件中的artifactId
    ![](img/img_3.png)
4.  新建一个spring boot项目
5.  在pom文件中导入依赖
    ![](img/img_4.png)
6.  启动类上添加注解@ComponentScan("com")
7.  启动类main函数中new SpringApplicationBuilder(application.class).web(WebApplicationType.NONE).run(args);
    ![](img/img_1.png)
8.  在Controller层写一个测试类写两个注解@Component @nettyController("/test") 一定要有@Component才能被扫描到
9.  支持复杂数据类型，但不支持嵌套情况  
    ![](img/img.png)
    ![](img/img_2.png)
10.  运行测试,在浏览器中输入localhost:端口号/test/hello?aaa=wuze&username=wuze&i=111
    ![](img/img_5.png)
11. 支持拦截器设置，拦截器需要继承IntercepterAdapter并重写channelRead()方法，在类上添加注解@nettyInterceptor @Component ，拦截通过则调用fireChannelRead(msg);方法
    ![](img/img_6.png)
